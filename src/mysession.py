# coding: utf-8
import pandas, scipy
get_ipython().run_line_magic('cd', 'git')
get_ipython().run_line_magic('cd', 'teledraw/src')
get_ipython().run_line_magic('ls', '')
df = pandas.read_csv("thymio.csv")
df
from scipy import interpolate
df2 = df.head(n=14)
df2
interpolate.interp1d(x=df2['Distance'], y=df2['Sensor'])
interpolation = _
import matplotlib.pyplot as plt
import numpy as np
xnew = np.arange(0, 13, 1)
xnew
xnew = np.arange(0, 14, 1)
xnew
ynew = interpolation(xnew)
get_ipython().run_line_magic('pinfo', 'plt.plot')
plt.plot(df2['Distance'], df2['Sensor'], 'o', xnew, ynew, '-')
plt.show()
interpolation.axis
interpolation.x
interpolation.y
int2 = interpolate.barycentric_interpolate(df2['Distance'], df2['Sensor'], 13)
int2
int2 = interpolate.barycentric_interpolate(df2['Distance'], df2['Sensor'], range(14))
int2
int2 = interpolate.BarycentricInterpolator(df2['Distance'], df2['Sensor'], range(14))
int2 = interpolate.BarycentricInterpolator(df2['Distance'], df2['Sensor'])
int2
ynew2 = int2(xnew)
plt.plot(df2['Distance'], df2['Sensor'], 'o', xnew, ynew2, '-')
plt.show
_()
plt.close()
int2.xi
int2.r
int2.wi
int2.dtype
int2(1)
int2(1.2)
get_ipython().run_line_magic('pinfo', 'int2.add_xi')
interpolator(1.23), int2(1.23)
interpolation(1.23), int2(1.23)
plt.show(
)
plt.show()
plt.plot(df2['Distance'], df2['Sensor'], 'o', xnew, ynew, 'x', xnew, ynew2, '-')
plt.show()
int3 = interpolate.PchipInterpolator(df2['Distance'], df2['Sensor'])
ynew3 = int3(xnew)
plt.plot(df2['Distance'], df2['Sensor'], 'o', ynew3, '-')
plt.show()
int3.derivative
int3.derivative()
int3
int3.c
get_ipython().run_line_magic('pinfo', 'int3.c')
get_ipython().run_line_magic('pinfo', 'int3')
get_ipython().run_line_magic('pinfo', 'np.arange')
get_ipython().run_line_magic('pinfo', 'np.linspace')
xnew
np.arange(0, 14)
np.arange(0, 14, 0.5)
xnew2 = np.linspace(0, 13, 100)
xnew
xnew2
plt.plot(df2['Distance'], df2['Sensor'], 'o', xnew2, int3(xnew2), '-')
plt.show()
plt.plot(df2['Distance'], df2['Sensor'], 'o', xnew2, interpolation(xnew2), '-', xnew2, int2(xnew2), '-', xnew2, int3(xnew2), '-')
plt.show()
xnew2 = np.linspace(0, 13, 10000)
plt.plot(df2['Distance'], df2['Sensor'], 'o', xnew2, interpolation(xnew2), '-', xnew2, int2(xnew2), '-', xnew2, int3(xnew2), '-')
plt.show()
get_ipython().run_line_magic('save', '')
get_ipython().run_line_magic('save', 'yay')
get_ipython().run_line_magic('save', "'yay'")
get_ipython().run_line_magic('pinfo', '%save')
get_ipython().run_line_magic('history', '')
get_ipython().run_line_magic('pinfo', '%save')
get_ipython().run_line_magic('save', "'yay' 1-80")
get_ipython().run_line_magic('save', 'mysession 1-99999999')
