#!/bin/sh

# Before running this controller GUI:
# $ asebaplayground thymio.playground
# $ asebamedulla -p 33337 'tcp:host=127.0.0.1;port=33333'

set -x
# asebaplayground thymio.playground 2>/dev/null &
# sleep 0.5s
# asebamedulla -p 33337 'tcp:host=127.0.0.1;port=33333' &
# sleep 2s

# entr -rs 'i3-msg focus right; asebaplayground thymio.playground&; sleep 0.5s; i3-msg focus left' <<<thymio.playground
# entr -rs "sleep 3s; echo -----; asebamedulla -p 33337 'tcp:host=127.0.0.1;port=33333'" <<<thymio.playground
# entr -rs 'sleep 5s; python test-medulla-kb.py $(realpath hello_aseba.aesl)&; sleep 3s; kill -9 $!' <<<thymio.playground

python3 test-medulla-kb.py "$(realpath hello_aseba.aesl)" "$@"
