#!/usr/bin/python
import dbus
import dbus.mainloop.glib
from gi.repository import GLib
import pygame
from pygame.locals import (KEYDOWN, QUIT, K_ESCAPE,
                           K_UP, K_DOWN, K_LEFT, K_RIGHT, K_1, K_2, K_3,
                           MOUSEBUTTONUP, MOUSEBUTTONDOWN, MOUSEMOTION)
from pygame import Vector2
import sys
from collections import namedtuple
import pprint


# Appearance
WINDOW_SIZE = (600, 600)
BG_COLOR = (39, 42, 51)
FG_COLOR = (240, 240, 250)
THYMIO_IMG = "thymio.png"
THYMIO_SIZE = (50, 50)
PROX_COLOR = (255, 255, 255)

# The length of the plotted "obstacle" lines
PROX_LINE_LENGTH = 10

# Minimum path segment length. A new segment is built when the user drags
# the mouse LINE_DIST px away from the last segment
LINE_DIST = 60

# How many pixels to map a distance unit in the Playground scene
TO_PIXELS_FACTOR = 5

# How fast (degrees per tick) should the sprite rotate to match Thymio's
# rotation speed in the Playground
SPRITE_SPEED = 3.5

# How fast (pixels per tick) should the sprite move to match Thymio's movement
# speed in the Playground
STEERING_SPEED = 7.2

# The amount of time in miliseconds to wait between each update loop
LOOP_PERIOD = 20

# Thymio's size in the Playeground is 12


def tuple_coords(vector):
    """Why is Vector2 rejected in most PyGame functions?"""
    return (int(vector[0]), int(vector[1]))


Sensor = namedtuple('Sensor', ['offset', 'angle'])


def front_sensor(dist, angle):
    """Thymio's front sensors are evenly spaced in a cylindrical surface"""
    offset = Vector2(dist, 0).rotate(angle)
    return Sensor(offset=tuple_coords(offset), angle=angle)


SENSOR_CONFIG = {
    # 5 front sensors
    **{i: front_sensor(THYMIO_SIZE[0] // 2, 15 * (i - 2)) for i in range(5)},

    # Rear sensors
    5: Sensor(offset=(-THYMIO_SIZE[0] // 2, THYMIO_SIZE[1] // 2), angle=180),
    6: Sensor(offset=(-THYMIO_SIZE[0] // 2, -THYMIO_SIZE[1] // 2), angle=180),
}


def sensor_to_sandbox_distance(x):
    """Convert a sensor reading to a distance in the Playground scene"""
    a, b, c, d = [2.81956584e+01, -1.63091265e-02,
                  4.44656821e-06, -4.87347197e-10]
    return a + b * x + c * (x**2) + d * (x**3)


class Path:
    """A sequence of commands to turn and move Thymio"""

    Segment = namedtuple('Segment', ['rotation', 'distance'])

    def __init__(self, initial_x, initial_y):
        self.segments = [Path.Segment(rotation=0, distance=1)]
        self.last_pos = Vector2(initial_x, initial_y)
        self.last_delta = Vector2(1, 0)

    def build_segment(self, point):
        """Derive the rotation and the distance needed to make the Thymio
        reach the given point after it has completed the previous segments"""
        delta = Vector2(point) - self.last_pos
        angle = delta.angle_to(self.last_delta)
        magnitude = delta.magnitude()
        self.segments.append(Path.Segment(rotation=angle, distance=magnitude))
        self.last_pos = Vector2(point)
        self.last_delta = delta

    def __repr__(self):
        return f'<Path({self.segments})>'


class MouseCanvas(pygame.sprite.Sprite):
    """A surface where the user traces a path for the Thymio to follow"""

    def __init__(self, controller):
        super().__init__()
        self.controller = controller
        self.mouse = self.pmouse = (0, 0)
        self.drawing = False
        self.points = []

        # Sprite fields
        self.image = pygame.Surface(WINDOW_SIZE, pygame.SRCALPHA)
        self.rect = pygame.Rect((0, 0), WINDOW_SIZE)

    def mouse_down(self, button):
        """Handle MOUSEBUTTONDOWN"""
        if not self.controller.thymio_sprite_exists():
            self.controller.put_thymio_sprite(*self.mouse)
            self.points = [self.mouse]
            self.path = Path(*self.mouse)
        else:
            self.drawing = True

    def mouse_up(self, button):
        """Handle MOUSEBUTTONUP"""
        self.drawing = False

    def mouse_move(self, buttons, pos, rel):
        """Handle MOUSEMOTION"""
        self.mouse = pos
        self.pmouse = (pos[0] - rel[0], pos[1] - rel[1])

        if self.drawing:
            self.process_drawing()

    def process_drawing(self):
        """Called every frame the mouse has moved while the mouse button
        is being held down, after the Thymio sprite has been positioned"""
        dist = Vector2(*self.points[-1]).distance_to(Vector2(*self.mouse))
        if dist >= LINE_DIST:
            self.points.append(self.mouse)
            pygame.draw.aaline(self.image, FG_COLOR,
                               self.points[-2], self.mouse)
            pygame.draw.circle(self.image, FG_COLOR, self.mouse, 10)

            txt = str(len(self.points) - 1)
            font = pygame.font.Font(None, 24)
            rendered_text = font.render(txt, True, BG_COLOR)
            blit_position = rendered_text.get_rect(center=self.points[-1])
            self.image.blit(rendered_text, blit_position)

            self.path.build_segment(self.mouse)

    def update(self):
        # Spam terminal
        pprint.pprint(self.path.segments)


class ThymioSprite(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.angle = 0
        img = pygame.image.load(THYMIO_IMG)
        scale_factor = THYMIO_SIZE[0] / img.get_size()[0]
        self.loaded_img = pygame.transform.rotozoom(img, -90, scale_factor)
        self.image = self.loaded_img.copy()
        self.rect = self.image.get_rect(center=(x, y))

    def move_and_steer(self, distance, angle):
        v = Vector2(distance, 0).rotate(-self.angle)
        self.rect.move_ip(tuple_coords(v))
        self.angle += angle

        self.image = pygame.transform.rotate(self.loaded_img, self.angle)
        self.rect = self.image.get_rect(center=self.rect.center)


class ProximityCanvas(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface(WINDOW_SIZE, pygame.SRCALPHA)
        self.rect = self.image.get_rect()

    def plot_wall(self, sensor_pos, angle, distance):
        origin = Vector2(sensor_pos)
        ray = Vector2(distance, 0).rotate(angle)
        hitpoint = origin + ray
        perp_ray = Vector2(ray.y, -ray.x).normalize() * PROX_LINE_LENGTH / 2
        line_from = tuple_coords(hitpoint - perp_ray)
        line_to = tuple_coords(hitpoint + perp_ray)
        pygame.draw.line(self.image, PROX_COLOR, line_from, line_to, 1)

    def clear(self):
        self.image.fill(BG_COLOR)

    def update(self):
        translucent_bg = pygame.Surface(WINDOW_SIZE)
        translucent_bg.fill(BG_COLOR)
        translucent_bg.set_alpha(1)
        self.image.blit(translucent_bg, (0, 0))


class ThymioController:
    """This class encapsulates the whole state of this application and manages
    a PyGame "game loop" while handling a GLib event loop for dbus-python to
    run the asyncronous method calls (Aseba's LoadScripts and SendEventName)"""

    def __init__(self, filename):
        # Tell dbus-python to use GLib's main loop (GLib.MainLoop())
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

        # Initialize all PyGame modules
        pygame.init()

        #
        # SPRITES
        #
        self.screen = self.show_screen()
        self.mouse_canvas = MouseCanvas(self)
        self.all_sprites = pygame.sprite.Group()

        #
        # CONTROLLER VARIABLES
        #
        # self.joystick = pygame.joystick.Joystick(0)
        # self.joystick.init()
        self.ox = 0
        self.oy = 0
        self.oc = [0] * 3

        # get stub of the Aseba network
        bus = dbus.SessionBus()
        asebaNetworkObject = bus.get_object('ch.epfl.mobots.Aseba', '/')
        self.asebaNetwork = dbus.Interface(
            asebaNetworkObject,
            dbus_interface='ch.epfl.mobots.AsebaNetwork'
        )

        # load the file
        self.asebaNetwork.LoadScripts(filename,
                                      reply_handler=self.dbusReply,
                                      error_handler=self.dbusError)

        # schedules first scan of joystick
        GLib.timeout_add(LOOP_PERIOD, self.process_events)

    def show_screen(self):
        """PyGame doesn't detect keys without an open window"""
        screen = pygame.display.set_mode(WINDOW_SIZE)
        screen.fill(BG_COLOR)
        return screen

    def thymio_sprite_exists(self):
        return hasattr(self, "thymio_sprite")

    def put_thymio_sprite(self, x, y):
        self.prox_canvas = ProximityCanvas()
        self.thymio_sprite = ThymioSprite(x, y)
        self.all_sprites.add(self.prox_canvas,
                             self.mouse_canvas,
                             self.thymio_sprite)

    def run(self):
        # run event loop
        self.loop = GLib.MainLoop()
        self.loop.run()

    def dbusReply(self):
        # correct replay on D-Bus, ignore
        pass

    def dbusError(self, e):
        # there was an error on D-Bus, stop loop
        print('dbus error: %s' % str(e))
        self.loop.quit()

    def process_events(self):
        # if no loop is running, skip function
        if not self.loop.is_running():
            return

        # scan joystick and send command to Thymio

        # pygame.event.pump()  # not needed because I'm using the event queue

        #
        # PYGAME EVENT QUEUE
        #
        for event in pygame.event.get():

            # Close the window
            if event.type == QUIT or (
                    event.type == KEYDOWN and event.key == K_ESCAPE):
                self.loop.quit()

            # Mouse handling
            elif event.type == MOUSEBUTTONUP:
                self.mouse_canvas.mouse_up(event.button)

            elif event.type == MOUSEBUTTONDOWN:
                self.mouse_canvas.mouse_down(event.button)
                self.prox_canvas.clear()

            elif event.type == MOUSEMOTION:
                self.mouse_canvas.mouse_move(event.buttons,
                                             event.pos, event.rel)

        # x = self.joystick.get_axis(0) * 300
        # y = -self.joystick.get_axis(1) * 300
        # c = [self.joystick.get_button(i) for i in range(3)]
        pressed_keys = pygame.key.get_pressed()
        in_x = 300 * (pressed_keys[K_RIGHT] - pressed_keys[K_LEFT])
        in_y = 300 * (pressed_keys[K_UP] - pressed_keys[K_DOWN])
        c = [pressed_keys[K_1], pressed_keys[K_2], pressed_keys[K_3]]
        # print(f'target = ( {in_x:3}, {in_y:3} )', end='  |  ')

        # send speed command
        if in_x != self.ox or in_y != self.oy:
            self.asebaNetwork.SendEventName('SetSpeed',
                                            [in_y + in_x, in_y - in_x],
                                            reply_handler=self.dbusReply,
                                            error_handler=self.dbusError
                                            )
            self.ox, self.oy = in_x, in_y

        # send color command
        if c != self.oc:
            self.asebaNetwork.SendEventName('SetColor',
                                            map(lambda x: 32 * x, c),
                                            reply_handler=self.dbusReply,
                                            error_handler=self.dbusError
                                            )
            self.oc = c

        # read motor speed
        # ml = self.asebaNetwork.GetVariable('thymio-II', 'motor.left.speed')
        # mr = self.asebaNetwork.GetVariable('thymio-II', 'motor.right.speed')
        # print(f'left: {ml[0]:3}   right: {mr[0]:3} )', end='  |  ')

        # read and display horizontal sensors
        horizontalProximity = self.asebaNetwork.GetVariable(
            'thymio-II', 'prox.horizontal')
        print(' '.join(['{:4}'] * 7).format(
            *map(str, horizontalProximity)), end='  |  ')
        print(*[f'{sensor_to_sandbox_distance(int(prox)):4.1f}'
                if int(prox) > 0 else '____'
                for prox in horizontalProximity], sep=', ')

        if self.thymio_sprite_exists():
            speed = in_y / 300 * SPRITE_SPEED
            angle = -in_x / 300 * STEERING_SPEED
            self.thymio_sprite.move_and_steer(speed, angle)
            thymio_pos = self.thymio_sprite.rect.center
            thymio_angle = self.thymio_sprite.angle
            for i, prox in enumerate(horizontalProximity):
                if prox == 0:
                    # Sensor didn't detect an obstacle. Don't plot anythoing
                    continue
                offset = SENSOR_CONFIG[i].offset
                offset = tuple_coords(Vector2(offset).rotate(-thymio_angle))
                angle = SENSOR_CONFIG[i].angle - thymio_angle
                sensor_pos = tuple_coords(Vector2(thymio_pos) + offset)
                distance = TO_PIXELS_FACTOR * sensor_to_sandbox_distance(prox)
                self.prox_canvas.plot_wall(sensor_pos, angle, distance)

        self.all_sprites.update()
        self.render()

        # reschedule scan of joystick
        GLib.timeout_add(LOOP_PERIOD, self.process_events)

    def render(self):
        """Render all the sprites"""
        self.screen.fill(BG_COLOR)
        self.all_sprites.draw(self.screen)
        self.render_text()
        pygame.display.flip()

    def render_text(self):
        """Render the top text that displays the relevant keyboard state"""
        kb_state = pygame.key.get_pressed()
        TEXT = (f'Up: {kb_state[K_UP]} '
                f'Down: {kb_state[K_DOWN]} '
                f'Left: {kb_state[K_LEFT]} '
                f'Right: {kb_state[K_RIGHT]} '
                f'1: {kb_state[K_1]} '
                f'2: {kb_state[K_2]} '
                f'3: {kb_state[K_3]} ')
        TEXT_FONT = pygame.font.Font(None, 24)  # default font
        TEXT_COLOR = pygame.Color('white')
        TEXT_POS = {'center': (WINDOW_SIZE[0] // 2, 20)}

        rendered_text = TEXT_FONT.render(TEXT, True, TEXT_COLOR)
        blit_position = rendered_text.get_rect(**TEXT_POS)
        self.screen.blit(rendered_text, blit_position)


def main():
    # check command-line arguments
    if len(sys.argv) != 2:
        print('Usage %s FILE' % sys.argv[0])
        return

    # Aseba's LoadScripts is dumb and I don't want to type $(realpath ...)
    # to pass an Aseba program to this script
    import pathlib
    filename = pathlib.Path(sys.argv[1]).expanduser().resolve().as_posix()

    # create and run controller
    thymioController = ThymioController(filename)
    thymioController.run()


if __name__ == '__main__':
    main()
